// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpenRequest); 
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSemiOpenRequest);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCloseRequest);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDINGESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();
	void OpenDoor();
	void SemiOpenDoor();
	void CloseDoor();
	float GetActorsOverlappingWeight();
	
	UPROPERTY(BlueprintAssignable)
	FOnOpenRequest OnOpenRequest;

	UPROPERTY(BlueprintAssignable)
	FOnOpenRequest OnSemiOpenRequest;

	UPROPERTY(BlueprintAssignable)
	FOnOpenRequest OnCloseRequest;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UPROPERTY(EditAnywhere)
	float WeightThreshold = 65;

	UPROPERTY(EditAnywhere)
	float OpenAngle = -90.f;

	float SemiOpenAngle = -10.f;

	float ClosedAngle = 0;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* PressurePlate = nullptr;
	
	AActor* ActorThatOpens = nullptr;

	UPROPERTY(EditAnywhere)
	float CloseDoorDelay = 1.f;

	float LastTimeOpenDelay;

	
};
