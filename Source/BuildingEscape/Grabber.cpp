// Fill out your copyright notice in the Description page of Project Settings.


#include "Grabber.h"


// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	// ...
	CheckComponents();
}

void UGrabber::CheckComponents() 
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (!PhysicsHandle) {  Log("PhysicsHandle"); return;  }

	Input = GetOwner()->FindComponentByClass<UInputComponent>();

	if (!Input) 
  		UE_LOG(LogTemp, Error, TEXT("Could not find Input Component in %s"), *GetOwner()->GetName())
	else 
	{
		Input->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		Input->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}

}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	MoveGrabbedObject();

}

void UGrabber::MoveGrabbedObject() 
{
   if (!PhysicsHandle) {  return;  }
	if (PhysicsHandle->GrabbedComponent)
	{
		FVector LineEndTrace = GetLineEnd();
		PhysicsHandle->SetTargetLocation(LineEndTrace);
	}
}

void UGrabber::DrawDebugTrace() 
{
	FVector LineEndTrace = GetLineEnd();

	DrawDebugLine(GetWorld(), PlayerViewPointLocation, LineEndTrace, FColor(255,255,0), false, DebugLineTime, 1.f, 4.f);
}

FVector UGrabber::GetLineEnd()
{
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		PlayerViewPointLocation,PlayerViewPointRotator);

	return PlayerViewPointLocation + PlayerViewPointRotator.Vector() * 100.0f;

}

void UGrabber::Grab()
{
	//if (Hit.GetActor())
	FHitResult Hit = GetBodyInReach();

	if (Hit.GetActor())
	{
		UE_LOG(LogTemp, Warning, TEXT("Grab %s"), *Hit.GetActor()->GetName());

		FVector LineEndTrace = GetLineEnd();
		
		if (!PhysicsHandle) { return;  }
		PhysicsHandle->GrabComponentAtLocationWithRotation(Hit.GetComponent(), NAME_None, LineEndTrace, Hit.GetActor()->GetActorRotation());
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("NO Component to Grab"));
	}
	
	
}

FHitResult UGrabber::GetBodyInReach()
{
	FVector LineEndTrace = GetLineEnd();
	FHitResult Hit;

	FCollisionQueryParams QueryParams(TEXT(""), false , GetOwner());

	GetWorld()->LineTraceSingleByObjectType(Hit, PlayerViewPointLocation, LineEndTrace, 
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody), QueryParams);
	
	return Hit;
}

void UGrabber::Release()
{
	if (!PhysicsHandle) {  Log("PhysicsHandle"); return;  }
	PhysicsHandle->ReleaseComponent();
}

void UGrabber::Log(FString MissingComponent) 
{
	UE_LOG(LogTemp, Error, TEXT("ERROR, Missing Component %s"), *MissingComponent);
}