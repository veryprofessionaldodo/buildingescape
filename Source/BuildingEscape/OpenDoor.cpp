// Fill out your copyright notice in the Description page of Project Settings.


#include "OpenDoor.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();

	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	// ...	
}

void UOpenDoor::OpenDoor() {
	//GetOwner()->SetActorRotation(FRotator(0, OpenAngle, 0));	
	
	UE_LOG(LogTemp, Warning, TEXT("PUTA"));

 	OnOpenRequest.Broadcast(); 

}

void UOpenDoor::CloseDoor() {
	
	//GetOwner()->SetActorRotation(FRotator(0, ClosedAngle, 0));

	OnCloseRequest.Broadcast(); 

}

void UOpenDoor::SemiOpenDoor() 
{
	OnSemiOpenRequest.Broadcast(); 
}

// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetActorsOverlappingWeight() > WeightThreshold) 
	{ 
		OpenDoor();
		LastTimeOpenDelay = GetWorld()->GetTimeSeconds();
		//UE_LOG(LogTemp, Warning, TEXT("Open Door"));
	} 
	else if (GetActorsOverlappingWeight() < WeightThreshold && GetActorsOverlappingWeight() > 0)
	{
		SemiOpenDoor();		
	}
	else if (GetWorld()->GetTimeSeconds() > (LastTimeOpenDelay + CloseDoorDelay)) 
	{
		CloseDoor();
		//UE_LOG(LogTemp, Warning, TEXT("Closed Door"));
	}
	

	// ...
}


float UOpenDoor::GetActorsOverlappingWeight() {

	float TotalMass = 0; 

	TArray<AActor*> OverlappingActors;

	if (!PressurePlate) {UE_LOG(LogTemp, Error, TEXT("Error, no Pressure Plate Assigned")); return 0; }
	PressurePlate->GetOverlappingActors(OverlappingActors);

	for (const auto& Actor : OverlappingActors)
	{
		if (PressurePlate->IsOverlappingActor(Actor))
		{
			TotalMass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
		}
	}

	return TotalMass;
}

